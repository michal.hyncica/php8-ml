<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\FunctionNotation\VoidReturnFixer;
use PhpCsFixer\Fixer\Whitespace\CompactNullableTypehintFixer;
use SlevomatCodingStandard\Sniffs\TypeHints\NullableTypeForNullDefaultValueSniff;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->import(__DIR__ . '/php70.php');

    $services = $containerConfigurator->services();

    $services->set('PhpCsFixer\Fixer\ClassNotation\VisibilityRequiredFixer')
        ->call('configure', [['elements' => ['const', 'property', 'method']]]);

    $services->set('PhpCsFixer\Fixer\ListNotation\ListSyntaxFixer')
        ->call('configure', [['syntax' => 'short']]);

    $services->set(NullableTypeForNullDefaultValueSniff::class);

    $services->set(CompactNullableTypehintFixer::class);

    $services->set(VoidReturnFixer::class);
};
