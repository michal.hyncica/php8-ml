<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->import(__DIR__ . '/../vendor/symplify/easy-coding-standard/config/set/php_codesniffer/php-codesniffer-psr2.php');

    $containerConfigurator->import(__DIR__ . '/../vendor/symplify/easy-coding-standard/config/set/php_cs_fixer/php-cs-fixer-psr2.php');
};
