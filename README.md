# PHP8-ML - PHP 8.0 port of PHP-ML library

<p align="center">
    <a href="https://gitlab.com/php-ai/php-ml">
	    <img src="https://gitlab.com/php-ai/php-ml/-/raw/master/docs/assets/php-ml-logo.png" alt="logo"/>
    </a>
</p>

This is a port of PHP-ML library (https://gitlab.com/php-ai/php-ml)
to make it work on PHP 8.
The original library was created by Arkadiusz Kondas (@ArkadiuszKondas).

This port has been created by Michal Hynčica

## PORT NOTES

### Done during port
- The error when `Phpml\Classification\Ensemble\Bagging` was creating the
  `Phpml\Classification\DecisionTree` instance has been fixed
- Fixed expected result in `SetTest::testToArray()` to reflect
  introduction of [stable sorting](https://wiki.php.net/rfc/stable_sorting)
  in php 8.
- The required version of `phpbench/phpbench` in composer.json has been 
  changed to `^1.0.0` so it can be installed on php 8. The phpbench isn't
  working currently. Its config need some more work.
- The required version of `symplify/easy-coding-standards`
  has been increased to `^9.4`. The config has been converted
  for newer version.
 

### Failing test
The test `DecisionStumpTest::testPredictSingleSample` is currently failing.
This is caused by introduction of [stable sorting](https://wiki.php.net/rfc/stable_sorting)
in php 8. The problem is in `Phpml\Classification\Classifier\OneVsRest::predictSample`
method. For non-binary decision stump this method gets probability 
for all classifiers. It uses `arsort()` to order label by its probability then
return the first label as most probable result.

In test case the probabilities looks like this:
```php
[
   '0' => 0.0,
   '1' => 1.0,
   '2' => 1.0,
];
```
Before introduction of stable sorting the order of elements with same value
wasn't well defined and by some luck the reverse sorted array looked like this:
```php
[
   '2' => 1.0,
   '1' => 1.0,
   '0' => 0.0,
];
```
But, with stable sorting introduced the reverse sorted array looks like this:
```php
[
   '1' => 1.0,
   '2' => 1.0,
   '0' => 0.0,
];
```
This is the cause the result is different from what is expected in test.


## Installation

Currently this library is in the process of being developed, but You can install it with Composer:

```
composer require hyncica/php8-ml
```

------------------------------------------
The rest is content of original README.md file. 

------------------------------------------
PHP-ML requires PHP >= 7.2.

Simple example of classification:
```php
require_once __DIR__ . '/vendor/autoload.php';

use Phpml\Classification\KNearestNeighbors;

$samples = [[1, 3], [1, 4], [2, 4], [3, 1], [4, 1], [4, 2]];
$labels = ['a', 'a', 'a', 'b', 'b', 'b'];

$classifier = new KNearestNeighbors();
$classifier->train($samples, $labels);

echo $classifier->predict([3, 2]);
// return 'b'
```

## Awards

<a href="http://www.yegor256.com/2016/10/23/award-2017.html">
  <img src="http://www.yegor256.com/images/award/2017/winner-itcraftsmanpl.png" width="400"/></a>

## Documentation

To find out how to use PHP-ML follow [Documentation](http://php-ml.readthedocs.org/).

## Examples

Example scripts are available in a separate repository [php-ai/php-ml-examples](https://github.com/php-ai/php-ml-examples).

## Datasets

Public datasets are available in a separate repository [php-ai/php-ml-datasets](https://github.com/php-ai/php-ml-datasets).

## Features

* Association rule learning
    * [Apriori](http://php-ml.readthedocs.io/en/latest/machine-learning/association/apriori/)
* Classification
    * [SVC](http://php-ml.readthedocs.io/en/latest/machine-learning/classification/svc/)
    * [k-Nearest Neighbors](http://php-ml.readthedocs.io/en/latest/machine-learning/classification/k-nearest-neighbors/)
    * [Naive Bayes](http://php-ml.readthedocs.io/en/latest/machine-learning/classification/naive-bayes/)
    * Decision Tree (CART)
    * Ensemble Algorithms
        * Bagging (Bootstrap Aggregating)
        * Random Forest
        * AdaBoost
    * Linear
        * Adaline
        * Decision Stump
        * Perceptron
        * LogisticRegression
* Regression
    * [Least Squares](http://php-ml.readthedocs.io/en/latest/machine-learning/regression/least-squares/)
    * [SVR](http://php-ml.readthedocs.io/en/latest/machine-learning/regression/svr/)
* Clustering
    * [k-Means](http://php-ml.readthedocs.io/en/latest/machine-learning/clustering/k-means/)
    * [DBSCAN](http://php-ml.readthedocs.io/en/latest/machine-learning/clustering/dbscan/)
    * Fuzzy C-Means
* Metric
    * [Accuracy](http://php-ml.readthedocs.io/en/latest/machine-learning/metric/accuracy/)
    * [Confusion Matrix](http://php-ml.readthedocs.io/en/latest/machine-learning/metric/confusion-matrix/)
    * [Classification Report](http://php-ml.readthedocs.io/en/latest/machine-learning/metric/classification-report/)
    * Regression
* Workflow
    * [Pipeline](http://php-ml.readthedocs.io/en/latest/machine-learning/workflow/pipeline)
    * FeatureUnion
* Neural Network
    * [Multilayer Perceptron Classifier](http://php-ml.readthedocs.io/en/latest/machine-learning/neural-network/multilayer-perceptron-classifier/)
* Cross Validation
    * [Random Split](http://php-ml.readthedocs.io/en/latest/machine-learning/cross-validation/random-split/)
    * [Stratified Random Split](http://php-ml.readthedocs.io/en/latest/machine-learning/cross-validation/stratified-random-split/)
* Feature Selection
    * [Variance Threshold](http://php-ml.readthedocs.io/en/latest/machine-learning/feature-selection/variance-threshold/)
    * [SelectKBest](http://php-ml.readthedocs.io/en/latest/machine-learning/feature-selection/selectkbest/)
* Preprocessing
    * [Normalization](http://php-ml.readthedocs.io/en/latest/machine-learning/preprocessing/normalization/)
    * [Imputation missing values](http://php-ml.readthedocs.io/en/latest/machine-learning/preprocessing/imputation-missing-values/)
    * LabelEncoder
    * LambdaTransformer
    * NumberConverter
    * ColumnFilter
    * OneHotEncoder
* Feature Extraction
    * [Token Count Vectorizer](http://php-ml.readthedocs.io/en/latest/machine-learning/feature-extraction/token-count-vectorizer/)
        * NGramTokenizer
        * WhitespaceTokenizer
        * WordTokenizer
    * [Tf-idf Transformer](http://php-ml.readthedocs.io/en/latest/machine-learning/feature-extraction/tf-idf-transformer/)
* Dimensionality Reduction
    * PCA (Principal Component Analysis)
    * Kernel PCA
    * LDA (Linear Discriminant Analysis)
* Datasets
    * [Array](http://php-ml.readthedocs.io/en/latest/machine-learning/datasets/array-dataset/)
    * [CSV](http://php-ml.readthedocs.io/en/latest/machine-learning/datasets/csv-dataset/)
    * [Files](http://php-ml.readthedocs.io/en/latest/machine-learning/datasets/files-dataset/)
    * [SVM](http://php-ml.readthedocs.io/en/latest/machine-learning/datasets/svm-dataset/)
    * [MNIST](http://php-ml.readthedocs.io/en/latest/machine-learning/datasets/mnist-dataset.md)
    * Ready to use:
        * [Iris](http://php-ml.readthedocs.io/en/latest/machine-learning/datasets/demo/iris/)
        * [Wine](http://php-ml.readthedocs.io/en/latest/machine-learning/datasets/demo/wine/)
        * [Glass](http://php-ml.readthedocs.io/en/latest/machine-learning/datasets/demo/glass/)
* Models management
    * [Persistency](http://php-ml.readthedocs.io/en/latest/machine-learning/model-manager/persistency/)
* Math
    * [Distance](http://php-ml.readthedocs.io/en/latest/math/distance/)
    * [Matrix](http://php-ml.readthedocs.io/en/latest/math/matrix/)
    * [Set](http://php-ml.readthedocs.io/en/latest/math/set/)
    * [Statistic](http://php-ml.readthedocs.io/en/latest/math/statistic/)
	* Linear Algebra

## Contribute

- [Guide: CONTRIBUTING.md](https://github.com/php-ai/php-ml/blob/master/CONTRIBUTING.md)
- [Issue Tracker: github.com/php-ai/php-ml](https://github.com/php-ai/php-ml/issues)
- [Source Code:  github.com/php-ai/php-ml](https://github.com/php-ai/php-ml)

You can find more about contributing in [CONTRIBUTING.md](CONTRIBUTING.md).

## License

PHP-ML is released under the MIT Licence. See the bundled LICENSE file for details.

## Author

Arkadiusz Kondas (@ArkadiuszKondas)
