<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\CastNotation\CastSpacesFixer;
use PhpCsFixer\Fixer\ClassNotation\MethodSeparationFixer;
use PhpCsFixer\Fixer\ClassNotation\NoBlankLinesAfterClassOpeningFixer;
use PhpCsFixer\Fixer\FunctionNotation\VoidReturnFixer;
use PhpCsFixer\Fixer\PhpTag\BlankLineAfterOpeningTagFixer;
use PhpCsFixer\Fixer\Phpdoc\PhpdocAlignFixer;
use PhpCsFixer\Fixer\Phpdoc\PhpdocSeparationFixer;
use PhpCsFixer\Fixer\Whitespace\BlankLineBeforeStatementFixer;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\CodingStandard\Fixer\ArrayNotation\StandaloneLineInMultilineArrayFixer;
use Symplify\CodingStandard\Fixer\Import\ImportNamespacedNameFixer;
use Symplify\CodingStandard\Fixer\Php\ClassStringToClassConstantFixer;
use Symplify\CodingStandard\Fixer\Property\ArrayPropertyDefaultValueFixer;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->import(__DIR__ . '/ecs/psr2.php');

    $containerConfigurator->import(__DIR__ . '/ecs/php71.php');

    $containerConfigurator->import(__DIR__ . '/vendor/symplify/easy-coding-standard/config/set/clean-code.php');

    $containerConfigurator->import(__DIR__ . '/vendor/symplify/easy-coding-standard/config/set/common.php');

    $services = $containerConfigurator->services();

    $services->set(BlankLineAfterOpeningTagFixer::class);

    $services->set(BlankLineBeforeStatementFixer::class);

    $services->set(CastSpacesFixer::class);

    $services->set('PhpCsFixer\Fixer\Operator\ConcatSpaceFixer')
        ->call('configure', [['spacing' => 'none']]);

    $services->set(MethodSeparationFixer::class);

    $services->set(NoBlankLinesAfterClassOpeningFixer::class);

    $services->set('PhpCsFixer\Fixer\Whitespace\NoSpacesAroundOffsetFixer')
        ->call('configure', [['positions' => ['inside', 'outside']]]);

    $services->set('PhpCsFixer\Fixer\Operator\BinaryOperatorSpacesFixer')
        ->call('configure', [['operators' => ['=>' => 'single_space', '=' => 'single_space']]]);

    $services->set('PhpCsFixer\Fixer\PhpUnit\PhpUnitTestCaseStaticMethodCallsFixer')
        ->call('configure', [['call_type' => 'self']]);

    $services->set(PhpdocSeparationFixer::class);

    $services->set(PhpdocAlignFixer::class);

    $services->set(ImportNamespacedNameFixer::class);

    $services->set(ClassStringToClassConstantFixer::class);

    $services->set(ArrayPropertyDefaultValueFixer::class);

    $services->set(StandaloneLineInMultilineArrayFixer::class);

    $parameters = $containerConfigurator->parameters();

    $parameters->set('skip', ['PhpCsFixer\Fixer\PhpUnit\PhpUnitStrictFixer' => null, 'PhpCsFixer\Fixer\Strict\StrictComparisonFixer' => null, 'PhpCsFixer\Fixer\Operator\NotOperatorWithSuccessorSpaceFixer' => null, 'PhpCsFixer\Fixer\Alias\RandomApiMigrationFixer' => ['src/CrossValidation/RandomSplit.php'], 'SlevomatCodingStandard\Sniffs\Classes\UnusedPrivateElementsSniff' => ['src/Preprocessing/Normalizer.php'], 'PhpCsFixer\Fixer\StringNotation\ExplicitStringVariableFixer' => ['src/Classification/DecisionTree/DecisionTreeLeaf.php'], 'Symplify\CodingStandard\Fixer\Commenting\RemoveUselessDocBlockFixer' => ['src/Helper/OneVsRest.php', 'src/Math/LinearAlgebra/LUDecomposition.php'], VoidReturnFixer::class => ['src/Classification/Linear/Perceptron.php'], 'SlevomatCodingStandard\Sniffs\TypeHints\TypeHintDeclarationSniff.MissingParameterTypeHint' => null, 'SlevomatCodingStandard\Sniffs\TypeHints\TypeHintDeclarationSniff.MissingTraversableParameterTypeHintSpecification' => null, 'SlevomatCodingStandard\Sniffs\TypeHints\TypeHintDeclarationSniff.MissingReturnTypeHint' => null, 'SlevomatCodingStandard\Sniffs\TypeHints\TypeHintDeclarationSniff.MissingTraversableReturnTypeHintSpecification' => null, 'SlevomatCodingStandard\Sniffs\TypeHints\TypeHintDeclarationSniff.MissingPropertyTypeHint' => null, 'SlevomatCodingStandard\Sniffs\TypeHints\TypeHintDeclarationSniff.MissingTraversablePropertyTypeHintSpecification' => null, 'PHP_CodeSniffer\Standards\Generic\Sniffs\CodeAnalysis\AssignmentInConditionSniff.FoundInWhileCondition' => null]);
};
